/* eslint-disable linebreak-style */
import React from 'react';
import { withRouter } from 'react-router-dom';
import useStyles from './styles.js';
import PropTypes from 'prop-types';
// Material UI
import Grid from '@material-ui/core/Grid';

const ViewTemplate = props => {
  const { history } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid
        className={classes.grid}
        container
      >
        <Grid
          className={classes.quoteContainer}
          item
          lg={5}
        >
          <>Hello</>
        </Grid>
        <Grid
          className={classes.content}
          item
          lg={7}
          xs={12}
        >
          <div className={classes.content}>
            <div className={classes.contentBody}>
              <>From Template</>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

ViewTemplate.propTypes = {
  history: PropTypes.object
};

export default withRouter(ViewTemplate);
