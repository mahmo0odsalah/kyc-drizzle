/* eslint-disable react/prop-types */
/* eslint-disable react/no-multi-comp */
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import ContractKYC from 'services/kyc.services';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActions,
  CardActionArea,
  Typography,
  Divider,
  Grid,
  Button,
  TextField,
  MenuItem
} from '@material-ui/core';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import NotificationsIcon from '@material-ui/icons/Notifications';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';

const useStyles = makeStyles(() => ({
  root: {},
  buttonRight: {
    float: 'right'
  }
}));

const Notification = props => {
  const classes = useStyles();
  const { text, actions, icon } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardContent>
          <Grid alignItems="center" container spacing={0}>
            <Grid item xs={1}>
              {icon || <NotificationsActiveIcon />}
            </Grid>
            <Grid item xs={10}>
              <Typography color="textSecondary" component="p" variant="body2">
                {text || "You've got a new message"}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
      <CardActions>{actions}</CardActions>
    </Card>
  );
};

const AccountDetails = props => {
  const [value, setValue] = React.useState(0);
  const { className, ...rest } = props;
  const [notifications, setNotifications] = useState([]);
  const classes = useStyles();

  const [values, setValues] = useState({
    bank: '',
    passport: '',
    license: '',
    utility: ''
  });

  useEffect(() => {
    setNotifications([
      <Notification text={'Your request to the bank is being processed.'} />,
      <Notification
        actions={[
          <Button color="primary" size="small">
            Accept
          </Button>,
          <Button color="primary" size="small">
            Reject
          </Button>
        ]}
        text={'A bank requested your data'}
      />
    ]);
  }, []);
  
  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const handleFileUpload = event => {
    event.preventDefault();
    setValues({
      ...values,
      [event.target.name]: event.target.files[0].name
    });
  };
  const handleSubmit = async () => {
    const kycService = await new ContractKYC(s=>console.log(s))
    const account = await kycService.getCurrentAccount();
    await kycService.addUser(
      account,
      'mariam',
      values.license,
      values.utility,
      values.passport
    )
    .then(res=>alert('success'))
    .catch(err=>alert(err.message || err))
  }
  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate>
        <BottomNavigation
          className={classes.root}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          showLabels
          value={value}>
          <BottomNavigationAction
            icon={<PersonOutlineIcon />}
            label="Profile"
          />
          <BottomNavigationAction icon={<NotificationsIcon />} label="Status" />
        </BottomNavigation>
        <Divider />
        <CardContent>
          <div hidden={value}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  helperText="Please specify the first name"
                  label="Bank"
                  margin="dense"
                  name="bank"
                  onChange={handleChange}
                  required
                  select
                  value={values.bank}
                  variant="outlined">
                  {[
                    { label: 'Beacon Bank', value: 'BB' },
                    { label: 'OltoBank', value: 'OB' },
                    { label: 'West Florida Bank', value: 'WFB' },
                    { label: 'Ace Bank', value: 'AB' }
                  ].map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12}>
                <Grid alignItems="center" container spacing={3}>
                  <Grid item xs={9}>
                    <TextField
                      disabled
                      fullWidth
                      helperText="Click Upload To Upload Your Passport"
                      label="Passport"
                      margin="dense"
                      required
                      value={values.passport}
                      variant="filled"
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="passport-button-file"
                      multiple
                      name="passport"
                      onChange={handleFileUpload}
                      required
                      style={{ display: 'none' }}
                      type="file"
                    />
                    <label htmlFor="passport-button-file">
                      <Button
                        className={classes.button}
                        component="span"
                        variant="text">
                        Upload
                      </Button>
                    </label>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid alignItems="center" container spacing={3}>
                  <Grid item xs={9}>
                    <TextField
                      disabled
                      fullWidth
                      helperText="Click Upload To Upload Your License"
                      label="License"
                      margin="dense"
                      required
                      value={values.license}
                      variant="filled"
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="license-button-file"
                      multiple
                      name="license"
                      onChange={handleFileUpload}
                      required
                      style={{ display: 'none' }}
                      type="file"
                    />
                    <label htmlFor="license-button-file">
                      <Button
                        className={classes.button}
                        component="span"
                        variant="text">
                        Upload
                      </Button>
                    </label>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid alignItems="center" container spacing={3}>
                  <Grid item xs={9}>
                    <TextField
                      disabled
                      fullWidth
                      helperText="Click Upload To Upload A Utility Bill"
                      label="utility bill"
                      margin="dense"
                      required
                      value={values.utility}
                      variant="filled"
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="utility-button-file"
                      multiple
                      name="utility"
                      onChange={handleFileUpload}
                      required
                      style={{ display: 'none' }}
                      type="file"
                    />
                    <label htmlFor="utility-button-file">
                      <Button
                        className={classes.button}
                        component="span"
                        variant="text">
                        Upload
                      </Button>
                    </label>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
          <div hidden={value !== 1}>
            <Grid container spacing={3}>
              {notifications.map(message => (
                <Grid item xs={12}>
                  {message}
                </Grid>
              ))}
            </Grid>
          </div>
        </CardContent>
        <Divider />
        <CardActions>
          <div hidden={value}>
            <Button
              color="primary"
              onClick={handleSubmit}
              variant="contained">
              Submit
            </Button>
          </div>
          <div hidden={value !== 1}>
            <Button
              color="primary"
              onClick={e => {
                setNotifications([]);
              }}
              variant="contained">
              Clear Notifications
            </Button>
          </div>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default AccountDetails;
