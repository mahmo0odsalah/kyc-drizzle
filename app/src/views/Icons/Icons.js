import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import ContractKYC from 'services/kyc.services';
// import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  // CardHeader,
  CardContent,
  // CardActions,
  // CardActionArea,
  // Typography,
  Divider,
  Grid,
  Button,
  TextField,
  // MenuItem
} from '@material-ui/core';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
// import NotificationsIcon from '@material-ui/icons/Notifications';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
// import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import SearchIcon from '@material-ui/icons/Search';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import Table from 'components/GenericTable';
import ImageGridList from './ImageGridList';

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(4)
  },
  grid: {
    marginLeft: theme.spacing(2)
  }
}));

const Icons = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [searchCustomer, setSearchCustomer] = useState('')
  useEffect(() => {
    (async()=>{
      const kycService = await new ContractKYC(s=>{
        console.log(s)
      })
      const account = await kycService.getCurrentAccount();
      await kycService.subscribeToApplications();
      console.log(kycService.getAppliedClients(account));
      //after getting applied clients we need to call getUserInfo on each of them to get the table data
    })(); 
  }, [])

  const handleSearch = async e=>{
    const kycService = await new ContractKYC(s=>{
      console.log(s)
    })
    const account = await kycService.getCurrentAccount();
    kycService.seeUserInfo(account,searchCustomer)
    .then(res=>console.log(res))
    .catch(err=> console.log(err))
  
  }
  return (
    <Card className={clsx(classes.root)}>
      <form autoComplete="off" noValidate>
        <BottomNavigation
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          showLabels
          className={classes.root}>
          <BottomNavigationAction
            label="Search New Customer"
            icon={<SearchIcon />}
          />
          <BottomNavigationAction label="KYC Approval" icon={<ThumbUpIcon />} />
          <BottomNavigationAction
            label="Loan Approval"
            icon={<LocalAtmIcon />}
          />
          <BottomNavigationAction
            label="Requested KYC"
            icon={<PersonOutlineIcon />}
          />
        </BottomNavigation>
        <Divider />
        <CardContent>
          <div hidden={value}>
            <Grid
              container
              className={classes.grid}
              spacing={3}
              alignItems="center">
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  label="Customer Name"
                  margin="dense"
                  required
                  variant="outlined"
                  onChange={e=>setSearchCustomer(e.target.value)}
                  value={searchCustomer}
                />
              </Grid>
              <Grid item xs={3}>
                <Button
                  variant="contained"
                  color="primary"
                  component="span"
                  className={classes.button}
                  onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
          </div>
          <div hidden={value !== 1}>
            <Table
              title="KYC requests"
              columns={[
                { title: 'Date', field: 'Date' },
                { title: 'Account No', field: 'AccountNo', type: 'numeric' },
                { title: 'Customer Name', field: 'name' },
                {
                  title: 'File Type',
                  field: 'fileType',
                  lookup: {
                    1: "Driver's license",
                    2: 'Employee ID',
                    3: 'Passport',
                    4: 'Utility Bill'
                  }
                },
                { title: 'status', field: 'status', default: 'pending' }
              ]}
              data={[
                {
                  Date: '3/4/2019',
                  AccountNo: '23127321372137',
                  name: 'Bill Cosby',
                  fileType: 1,
                  status: 'pending'
                },
                {
                  Date: '3/4/2020',
                  AccountNo: '23121321372137',
                  name: 'Kevin De Bruyne',
                  fileType: 2,
                  status: 'approved'
                }
              ]}
              actions={[
                rowData => ({
                  icon: 'save',
                  tooltip: 'Approve User',
                  hidden: rowData.status === 'approved',
                  onClick: async (event, rowData) =>{
                    const kycService = await new ContractKYC(s=>{
                      console.log(s)
                    })
                    const account = await kycService.getCurrentAccount();
                    kycService.acceptUser(account,rowData.AccountNo)
                    .then(res=>alert(res))
                    .catch(err=> alert(err))
                  }
                }),
                rowData => ({
                  icon: 'close',
                  tooltip: 'Reject User',
                  hidden: rowData.status === 'approved',
                  onClick: (event, rowData) =>
                    alert('You rejected ' + rowData.name)
                })
              ]}></Table>
          </div>
          <div hidden={value !== 3}>
            <ImageGridList />
          </div>
        </CardContent>
        <Divider />
      </form>
    </Card>
  );
};

export default Icons;
