import license from 'assets/license.jpg';
import bill from 'assets/bill.jpg';
import passport from 'assets/passport.jpg';

const tileData = [
  {
    img: license,
    title: 'Driver License',
    cols: 2,
    featured: true
  },
  {
    img: passport,
    title: 'Passport'
  },
  {
    img: bill,
    title: 'Utility Bill'
  }
];

export default tileData;
