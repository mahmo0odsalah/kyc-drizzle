// to create a new form configuration just export a const of an array
// this array contain the objects that descripe each field
// each object should contain the following properties
//      'name', 'label', 'type', 'placeholder',  'variant',
//       'options (if input is a select input)'
// don't forget to export in index.js
const exampleFormConfigs = [
  {
    name: 'field name, type of String',
    label: 'field label, type of String',
    type: 'field type: textField/selectField/checkBox, type of String',
    placeholder: 'field placeholder, type of String',
    validators: 'field validators, type of Array',
    variant: 'field variant, type of String',
    options: 'field options for select input, type of Array'
  },
  // options is an array of opjects, each object is in the format  { label: 'Label', value: 'value' },
  {
    name: '',
    label: '',
    type: '',
    placeholder: '',
    validators: [],
    variant: '',
    options: [{ label: 'Lable', value: 'value' }]
  }
];

export default exampleFormConfigs;
