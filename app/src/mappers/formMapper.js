import React from 'react';
import TextInput from '../components/FormComponents/TextInput';
import SelectInput from '../components/FormComponents/SelectInput';

export const formMapper = (configs, register, classes) => {
  return configs.map(element => {
    if (element.type === 'textField' || element.type === 'password')
      return (
        <TextInput
          classes={classes[element.type]}
          inputRef={register({ required: true })}
          key={`${element.label + element.type}`}
          label={element.label}
          name={element.name}
          placeholder={element.placeholder}
          type={element.type}
          variant={element.variant}
        />
      );
    else if (element.type === 'select')
      return (
        <SelectInput
          classes={classes[element.type]}
          inputRef={register({ required: true })}
          key={`${element.label + element.type}`}
          label={element.label}
          name={element.name}
          options={element.options}
          placeholder={element.placeholder}
          variant={element.variant}
        />
      );
  });
};
