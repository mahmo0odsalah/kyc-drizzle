import Web3 from "web3";
import SimpleStorage from "./contracts/SimpleStorage.json";
import ComplexStorage from "./contracts/ComplexStorage.json";
import TutorialToken from "./contracts/TutorialToken.json";
import kyc from "./contracts/kyc.json";

const options = {
  web3: {
    block: false,
    customProvider: new Web3("http://49.12.9.22:8545")
  },
  contracts: [kyc],
  events: {
    // SimpleStorage: ["StorageSet"]
  },
  polls: {
    accounts: 1500
  }
};

export default options;
