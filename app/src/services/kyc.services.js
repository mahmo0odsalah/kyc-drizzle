import web3 from 'web3';
import contract from 'truffle-contract';
import contractArtifact from './kyc.json';
window.web3 = web3;

let applications = {};
class ContractKYC {
  constructor () {
    if (window.ethereum) {
      this.web3Provider = window.ethereum;
      try {
        // Request account access
        window.ethereum.enable();
      } catch (error) {
        // User denied account access...
        console.error("User denied account access");
      }
    } else if (window.web3) {
      this.web3Provider = window.web3.currentProvider;
    } else {  
    web3.providers.HttpProvider.prototype.sendAsync =
      web3.providers.HttpProvider.prototype.send;
    this.web3Provider = new web3.providers.HttpProvider(
      // 'http://54.158.43.146:22000'
      // 'http://3.135.200.101:22000'
      'http://49.12.9.22:8545'
    );
    }
    this.web3 = new web3(this.web3Provider);
    console.log('HTTP provider');
    this.initContract().then(s => {
      console.log(s, 'init contract');
    });
  }
  async getCurrentAccount() {
    this.accounts = await this.web3.eth.getAccounts();
    if (this.accounts.length === 0) {
      alert(
        'Couldn\'t get any this.accounts! Make sure your Ethereum client is configured correctly.'
      );
      return;
    }
    console.log(this.accounts, 'this.accounts');
    this.account = this.accounts[0];
    return this.account;
  }
  async initContract() {
    // Get the necessary contract artifact file and instantiate it with truffle-contract.
    this.contract = contract(contractArtifact);
    // console.log(contractArtifact, 'contractArtifact');
    // Set the provider for our contract.
    try{
      this.contract.setProvider(this.web3Provider);
    }
    catch( err){
      alert('Please Make Sure Meta Mask Is Installed')
    }
    await this.getCurrentAccount();
  }
  getAppliedClients(bank){
    return applications[bank];
  }

  //subscribe 
  async subscribeToApplications(){
    const instance = await this.contract.deployed();
    await instance
      .UserSendRequest()
      .on('data', log => {
        applications[log.returnValues._bank] = applications[log.returnValues._bank].concat(log.returnValues._user);
      })
  }
  /// Getter Functions
  async seeMyInfo(sender) {
    const instance = await this.contract.deployed();
    const data = instance.seeMyInfo.call({ from: sender });
    return data;
  }

  async seeUserInfo(sender, user) {
    const instance = await this.contract.deployed();
    const data = instance.seeUserInfo.call(user, { from: sender });
    return data;
  }

  /// Setter Functions
  async addUser(sender, name, drivingLicense, utilityBill, passport) {
    const instance = await this.contract.deployed();
    const data = await instance.addUser(name, drivingLicense, utilityBill, passport, { from: sender, gas: 2000000 });
    return data;
  }

  async userRequest(sender, bank) {
    const instance = await this.contract.deployed();
    const data = await instance.userRequest(bank, { from: sender, gas: 2000000 });
    return data;
  }

  async acceptUser(sender, user) {
    const instance = await this.contract.deployed();
    const data = await instance.acceptUser(user, { from: sender, gas: 2000000 });
    return data;
  }
}

export default ContractKYC;
