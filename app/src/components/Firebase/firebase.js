import app from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyCdxWjF_K2DGHCgafX4iNeLNgkJkt9R2-U",
  authDomain: "kyc-5b552.firebaseapp.com",
  databaseURL: "https://kyc-5b552.firebaseio.com",
  projectId: "kyc-5b552",
  storageBucket: "kyc-5b552.appspot.com",
  messagingSenderId: "1015677528984",
  appId: "1:1015677528984:web:240d67a64f3685aa5b19ed",
  measurementId: "G-Q0B6Z5QZXT"
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();

  }

   // *** Auth API ***
   doCreateUserWithEmailAndPassword = (email, password,displayName) =>
     this.auth.createUserWithEmailAndPassword(email, password)
       .then(res=>{
         const user = this.auth.currentUser;
         return user.updateProfile({
           displayName:displayName
         })
       })
       .then(res=>{
         return Promise.resolve()
       })
       .catch(err=> {
        return Promise.reject(err)
       })
     

   doSignInWithEmailAndPassword = (email, password) =>
     this.auth.signInWithEmailAndPassword(email, password);

   doSignOut = () => this.auth.signOut();

   doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
   
   doPasswordUpdate = password =>
     this.auth.currentUser.updatePassword(password);

   getUser = ()=>{
     const user = this.auth.currentUser;
     user.sendEmailVerification().then(function() {
       Promise.resolve()
     }).catch(function(error) {
       Promise.reject(error)
     });

   }
   
}
export default Firebase;