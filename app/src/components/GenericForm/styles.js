import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  form: {
    paddingLeft: 100,
    paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1),
    color: 'primary'
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  password: {
    marginTop: theme.spacing(2)
  },
  select: {
    marginTop: theme.spacing(2)
  },
  policy: {
    marginTop: theme.spacing(1),
    display: 'flex',
    alignItems: 'center'
  },
  policyCheckbox: {
    marginLeft: '-14px'
  },
  submitButton: {
    margin: theme.spacing(2, 0)
  },
  secondaryActionButton: {
    marginTop: '-8px'
  }
}));

export default useStyles;
