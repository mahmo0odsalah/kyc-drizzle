import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import useForm from 'react-hook-form';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import { formMapper } from 'mappers/formMapper';
import useStyles from './styles';

/**
 * @namespace GenericForm
 * @memberof app.GenericForm
 *
 * @description
 * Component for generating forms using static configs, formMapper and form input components,
 * the components depends on formMapper.js and form inputs components
 *
 * @param {object} props Properties
 * @prop {object} props - the props that are passed to this component
 * @prop {string} props.formTitle - the title of the form
 * @prop {function} props.serviceFunc - the onSubmit function
 * @prop {string} props.formSecondaryTitle - form secondary title shown underneath the formtitle
 * @prop {string} props.submitButtonText - text inside submit button
 * @prop {string} props.secondaryActionButtonText - text for secondary action button if not provided form doesn't have secondary action
 * @prop {function} props.secondaryActionButtonFunc - function for secondary action button onClick()
 * @prop {string} props.preRedirectTip - text can provide a text tip to redirect user - useful for signin and signup pages
 * @prop {string} props.redirectTipLink - link of redirect should be in the format '/example-redirect'
 * @prop {string} props.postRedirectTip - optional complement for redirect tip could, appears at line's end of the redirect tip
 * @prop {object} props.formAdditionalComponent - additional optional react component could be added to the form
 * @returns {*}
 *
 *
 */

const GenericForm = ({
  formTitle,
  serviceFunc,
  formSecondaryTitle,
  formConfigs,
  submitButtonText,
  secondaryActionButtonText,
  secondaryActionButtonFunc,
  preRedirectTip,
  redirectTipLinkText,
  redirectTipLink,
  postRedirectTip,
  formAdditionalComponent
}) => {
  const { register, handleSubmit } = useForm();
  const classes = useStyles();
  const onSubmit = data => {
    serviceFunc(data);
  };

  return (
    <form
      className={classes.form}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Typography
        className={classes.title}
        variant="h3"
      >
        {formTitle}
      </Typography>

      <Typography
        color="textSecondary"
        gutterBottom
      >
        {formSecondaryTitle}
      </Typography>

      {formMapper(formConfigs, register, classes)}

      {formAdditionalComponent}

      <Button
        className={classes.submitButton}
        color="primary"
        // disabled={!formState.isValid}
        fullWidth
        size="large"
        type="submit"
        variant="contained"
      >
        {submitButtonText}
      </Button>
      {secondaryActionButtonText ? (
        <Button
          className={classes.secondaryActionButton}
          color="primary"
          // disabled={!formState.isValid}
          fullWidth
          onClick={secondaryActionButtonFunc}
          size="large"
          variant="text"
        >
          {secondaryActionButtonText}
        </Button>
      ) : null}

      {redirectTipLink ? (
        <Typography
          color="textSecondary"
          variant="body1"
        >
          {preRedirectTip}{' '}
          <Link
            component={RouterLink}
            to={redirectTipLink}
            variant="h6"
          >
            {redirectTipLinkText}
          </Link>{' '}
          {postRedirectTip}
        </Typography>
      ) : null}
    </form>
  );
};

GenericForm.propTypes = {
  classes: PropTypes.object,
  formAdditionalComponent: PropTypes.object,
  formConfigs: PropTypes.array.isRequired,
  formSecondaryTitle: PropTypes.string,
  formTitle: PropTypes.string.isRequired,
  postRedirectTip: PropTypes.string,
  preRedirectTip: PropTypes.string,
  redirectTipLink: PropTypes.string,
  redirectTipLinkText: PropTypes.string,
  secondaryActionButtonFunc: PropTypes.func,
  secondaryActionButtonText: PropTypes.string,
  serviceFunc: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired
};

export default GenericForm;
