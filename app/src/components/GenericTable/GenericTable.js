import React from 'react';
import PropTypes from 'prop-types';
import MaterialTable from 'material-table';

/**
 * @namespace GenericTable
 * @memberof app.GenericTable
 *
 * @description
 * Component for generating tables
 *
 * @param {object} props Properties
 * @prop {object} props - the props that are passed to this component
 * @prop {string} props.title - table title
 * @prop {array} props.columns - table columns configs
 * @prop {array} props.data - table data
 * @prop {array} props.actions - array of table custom actions
 * @returns {*}
 *
 *
 */

const GenericTable = props => {
  const { title, columns, data, actions } = props;

  return (
    <MaterialTable
      actions={actions}
      columns={columns}
      data={data}
      title={title}
    />
  );
};

GenericTable.propTypes = {
  actions: PropTypes.array.isRequired,
  className: PropTypes.string,
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  title: PropTypes.string
};

export default GenericTable;
