import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { SearchInput } from 'components';
import useStyles from '../../styles';

const TableToolbar = props => {
  const { children } = props;
  const classes = useStyles();
  return (
    <Grid
      alignItems="center"
      container
      direction="row"
      justify="space-between"
    >
      <Grid
        item
        lg={8}
        md={8}
        sm={8}
        xs={8}
      >
        <div className={classes.row}>
          <SearchInput
            className={classes.searchInput}
            placeholder="Search user"
          />
        </div>
      </Grid>
      {children}
    </Grid>
  );
};

TableToolbar.propTypes = {
  children: PropTypes.object
};

export default TableToolbar;
