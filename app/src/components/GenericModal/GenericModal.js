import React, { useState } from 'react';
import useStyles from './styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

/**
 * @namespace GenericModal
 * @memberof app.GenericModal
 *
 * @description
 * Component for wrapping any react children inside a modal triggered by another button, this component also renders a button not only the modal
 *
 * @param {object} props Properties
 * @prop {object} props - the props that are passed to this component
 * @prop {object} props.children - another node passed to the component in order to render inside the modal body
 * @prop {string} props.actionButtonText - text inside the button rendered
 * @prop {string} props.buttonVariant - button variant, could be "contained, outlined, text" (optional)
 * @prop {string} props.buttonColor - button color, could be "primary, secondary, inherit" (optional)
 * @returns {*}
 *
 *
 */

const GenericModal = ({
  children,
  actionButtonText,
  buttonVariant,
  buttonColor
}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Button
        color={buttonColor || 'primary'}
        onClick={handleOpen}
        variant={buttonVariant || 'contained'}>
        {actionButtonText}
      </Button>
      <div>
        <Modal
          aria-describedby="simple-modal-description"
          aria-labelledby="simple-modal-title"
          onClose={handleClose}
          open={open}>
          <div className={classes.paper}>
            {/* <Button onClick={handleClose}>Close</Button> */}

            {children}
          </div>
        </Modal>
      </div>
    </>
  );
};

GenericModal.propTypes = {
  actionButtonText: PropTypes.object.isRequired,
  buttonColor: PropTypes.string,
  buttonVariant: PropTypes.string,
  children: PropTypes.object,
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

export default GenericModal;
