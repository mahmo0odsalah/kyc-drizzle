/* eslint-disable no-console */
import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Dropzone from './Dropzone/Dropzone';
import LinearProgress from '@material-ui/core/LinearProgress';
import uploadFile from '../../../services/fileUpload/fileUpload';
import useStyles from './styles';

const FileInput = props => {
  const classes = useStyles();
  const [files, setFiles] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [uploadProgress, setUploadProgress] = useState({});
  const [isFullUploaded, setIsFullUploaded] = useState(false);

  const onFilesAdded = files => {
    setFiles(prevFiles => prevFiles.concat(files));
  };

  const uploadFiles = async () => {
    setUploadProgress({});
    setUploading(false);
    const promises = [];
    files.forEach(file => {
      promises.push(uploadFile(file));
    });

    try {
      await Promise.all(promises);
      setIsFullUploaded(true);
      setUploading(false);
    } catch (error) {
      console.error(error);
      setIsFullUploaded(false);
      setUploading(false);
    }
  };

  const calculateProgress = file => {
    const uploadProgress = setUploadProgress[file.name];
    if (uploading || isFullUploaded) {
      // logic for calculating uploading progress, depends on event handler's loaded and total values
    }
  };
  return (
    <Grid
      container
      direction="row"
      justify="space-between"
    >
      <Grid item>
        <Dropzone
          disabled={uploading || isFullUploaded}
          onFilesAdded={onFilesAdded}
        />
      </Grid>
      <Grid
        alignItems="stretch"
        item
      >
        <Grid
          className={classes.filesContainer}
          container
          direction="column"
        >
          {files.map(file => (
            <Grid
              item
              key={file.name}
            >
              <LinearProgress
                value={uploadProgress[file.name]}
                variant="determinate"
              />
              <Typography className={classes.fileName}>{file.name}</Typography>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default FileInput;
