import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles';

const Dropzone = props => {
  const classes = useStyles();
  const [highlight, setHighlight] = useState(false);

  let fileInputRef = React.createRef();

  const openFileDialog = () => {
    if (props.disabled) return;
    fileInputRef.current.click();
  };

  const onFilesAdded = e => {
    if (props.disabled) return;

    const files = e.target.files;
    if (props.onFilesAdded) {
      const array = fileListToArray(files);
      props.onFilesAdded(array);
    }
  };

  // const onDragOver = e => {
  //   e.preventDefault();
  //   if (props.disabled) return;
  //   setHighlight(true);
  // };

  // const onDragLeave = e => {
  //   e.preventDefault();
  //   setHighlight(false);
  // };

  // const onDrop = e => {
  //   e.preventDefault();
  //   if (props.disabled) return;

  //   const files = e.dataTransfer.files;
  //   if (props.onFilesAdded) {
  //     const array = fileListToArray(files);
  //     props.onFilesAdded(array);
  //   }
  //   setHighlight(false);
  // };

  const fileListToArray = list => {
    const array = [];
    for (let i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  };

  return (
    <div
      className={highlight ? classes.highlight : classes.dropzone}
      onClick={openFileDialog}
      // onDragLeave={onDragLeave}
      // onDragOver={onDragOver}
      // onDrop={onDrop}
      style={{ cursor: props.disabled ? 'default' : 'pointer' }}
    >
      <CloudUploadIcon className={classes.icon} />
      <input
        className={classes.fileInput}
        multiple
        onChange={onFilesAdded}
        ref={fileInputRef}
        type="file"
      />
      <Typography variant="caption">Upload Files</Typography>
    </div>
  );
};

Dropzone.propTypes = {
  disabled: PropTypes.bool.isRequired,
  onFilesAdded: PropTypes.func.isRequired
};

export default Dropzone;
