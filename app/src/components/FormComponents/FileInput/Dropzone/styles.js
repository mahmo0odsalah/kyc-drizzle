import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  dropzone: {
    height: '100px',
    width: '100px',
    backgroundColor: '#fff',
    border: '2px dashed rgb(187, 186, 189)',
    borderRadius: '5%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    fontSize: '16px',
    marginTop: '15px'
  },
  highlight: {
    backgroundColor: 'rgb(188,185,236)'
  },
  icon: {
    opacity: '0.3',
    height: '40px',
    width: '40px'
  },
  fileInput: {
    display: 'none'
  }
}));

export default useStyles;
