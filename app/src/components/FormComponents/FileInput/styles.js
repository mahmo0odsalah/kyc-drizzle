import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  filesContainer: {
    marginTop: '20px'
  },
  fileName: {
    margin: '10px 0px 10px 0px'
  }
}));

export default useStyles;
