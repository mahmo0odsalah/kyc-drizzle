/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

const SelectInput = ({
  classes,
  error,
  label,
  name,
  variant,
  options,
  inputRef,
  placeholder
}) => {
  const [asyncOptions, setAsyncOptions] = useState([]);
  useEffect(() => {
    if (Array.isArray(options)) setAsyncOptions(options);
    else {
      options
        .then(options => setAsyncOptions(options))
        .catch(error => console.error(error));
    }
  });
  return (
    <TextField
      className={classes}
      error={error}
      fullWidth
      inputRef={inputRef}
      label={label}
      margin="dense"
      name={name}
      placeholder={placeholder}
      select
      // eslint-disable-next-line react/jsx-sort-props
      SelectProps={{ native: true }}
      variant={variant}
    >
      <option value={' '}>{''}</option>
      {asyncOptions.map(({ label, value }) => (
        <option
          key={`key-${value}`}
          value={value}
        >
          {label}
        </option>
      ))}
    </TextField>
  );
};

SelectInput.propTypes = {
  classes: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  inputRef: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  placeholder: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired
};

export default SelectInput;
