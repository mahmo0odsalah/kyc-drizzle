import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const DateInput = ({ classes, inputRef, label, name, variant }) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around">
        <KeyboardDatePicker
          className={classes}
          disableToolbar
          variant={variant}
          name={name}
          fullWidth
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label={label}
          KeyboardButtonProps={{
            'aria-label': 'change date'
          }}
          inputRef={inputRef}
        />
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

DateInput.propTypes = {
  classes: PropTypes.string.isRequired,
  inputRef: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired
};
export default DateInput;
