import React from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

const TextInput = ({
  classes,
  error,
  label,
  name,
  variant,
  inputRef,
  placeholder,
  type
}) => {
  return (
    <TextField
      className={classes}
      error={error}
      fullWidth
      inputRef={inputRef}
      label={label}
      name={name}
      placeholder={placeholder}
      type={type}
      variant={variant}
    />
  );
};

TextInput.propTypes = {
  classes: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  inputRef: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired
};

export default TextInput;
