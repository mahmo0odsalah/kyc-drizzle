import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography, Grid, LinearProgress } from '@material-ui/core';
import FileIcon from '@material-ui/icons/FileCopy';
import AddIcon from '@material-ui/icons/AddCircle';
import sha256 from 'sha256';

const DocumentsFileInput = ({ title, submitFunc }) => {
  const [fileIsSelected, setFileIsSelected] = useState(false);
  const [file, setFile] = useState(null);
  const [progress, setProgress] = useState(0);

  let fileInputRef = React.createRef();
  const openFileDialog = () => {
    fileInputRef.current.click();
  };
  const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  const onFileAdded = e => {
    const file = e.target.files[0];
    const name = file && file.name;
    const fileData = { fileInByte: '', fileKey: '', fileName: '' };
    const reader = new FileReader();

    fileData.fileName = name;

    reader.readAsArrayBuffer(file);
    reader.onLoad = (() => {
      let typedarray = new Uint8Array(reader.result);
      fileData.fileHash = sha256(typedarray);
      fileData.fileInByte = typedarray;

      const formattedFileObject = { key: name, data: fileData };

      setFile(formattedFileObject);
      submitFunc(formattedFileObject);
      setProgress(100);
    })();

    setFileIsSelected(true);
  };

  return (
    <Paper
      style={{
        padding: '20px 20px',
        margin: '20px 20px',
        width: '680px',
        height: '160px'
      }}>
      <input
        style={{ display: 'none' }}
        onChange={onFileAdded}
        ref={fileInputRef}
        type="file"
      />
      <Grid container direction="column">
        <Grid item style={{ margin: '10px 0px' }}>
          <Typography variant="h3" component="h3">
            {title}
          </Typography>
        </Grid>

        {fileIsSelected ? (
          <Grid item>
            <Grid container direction="row">
              <Grid item style={{ marginTop: '10px' }}>
                <FileIcon fontSize="large" />
              </Grid>
              <Grid
                item
                style={{
                  minWidth: '90%',
                  marginTop: '10px',
                  marginLeft: '10px'
                }}>
                <Grid container direction="column">
                  <Grid item style={{ marginBottom: '7px' }}>
                    <Typography variant="h5" component="h5">
                      {file && file.key && file.key.substring(0, 40) + '...'}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <LinearProgress variant="determinate" value={progress} />
                  </Grid>
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      style={{ marginTop: '5px' }}>
                      <Grid item>
                        <Typography variant="caption"></Typography>
                      </Grid>
                      <Grid item>
                        <Typography variant="caption">{progress}%</Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        ) : (
          <Grid item>
            <Grid
              container
              direction="row"
              style={{ marginTop: '5px', cursor: 'pointer' }}
              onClick={() => {
                openFileDialog();
              }}>
              <Grid item>
                <AddIcon fontSize="large" color="primary" />
              </Grid>
              <Grid item style={{ marginTop: '5px', marginLeft: '5px' }}>
                <Typography variant="h4" component="h4">
                  Upload File
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>
    </Paper>
  );
};

DocumentsFileInput.propTypes = {};

export default DocumentsFileInput;
