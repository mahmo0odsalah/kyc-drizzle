pragma solidity >=0.5.2 <0.6.0;

contract kyc {
    struct UserInfo {
        bool exists;
        string name;
        string drivingLicense;
        string utilityBill;
        string passport;
        address[] banks;
        // address[] requests;
        mapping(address => bool) bankAccepted;
        mapping(address => bool) bankRequested;
    }

    event UserSendRequest(address _user, address indexed _bank);
    event BankAcceptRequest(address _user, address indexed _bank);

    mapping(address => UserInfo) userToInfo;

    modifier notAdded() {
        require(
            !userToInfo[msg.sender].exists,
            "A user with the provided address already exists."
        );
        _;
    }

    modifier isUser(address _user) {
        require(
            userToInfo[_user].exists,
            "A user with the provided address doesn't exist"
        );
        _;
    }

    modifier accessToUser(address _user) {
        // UserInfo memory userInfo = userToInfo[_user];

        // bool isAuthorized = false;
        // for (uint i = 0; i < userInfo.banks.length; i++){
        //     isAuthorized = isAuthorized || (userInfo.banks[i] == msg.sender) || _user == msg.sender;
        //     if(isAuthorized) break;
        // }
        bool access = userToInfo[_user].bankRequested[msg.sender] ||
            userToInfo[_user].bankAccepted[msg.sender];
        require(
            access,
            "You don't have the authority to view this user's info."
        );
        _;
    }

    function addUser(
        string memory _name,
        string memory _drivingLicense,
        string memory _utilityBill,
        string memory _passport
    ) public notAdded {
        userToInfo[msg.sender] = UserInfo(
            true,
            _name,
            _drivingLicense,
            _utilityBill,
            _passport,
            new address[](0)
        );
    }

    function userRequest(address _bank) public isUser(msg.sender) {
        // userToInfo[msg.sender].requests.push(_bank);
        userToInfo[msg.sender].bankRequested[_bank] = true;
        emit UserSendRequest(msg.sender, _bank);
    }

    // function removeAuthority(address _user, address _bank) internal {
    //     address[] storage banks = userToInfo[_user].banks;

    //     for (uint i = 0; i<banks.length; i++){
    //         if(banks[i] == _bank){
    //             delete banks[i];
    //             break;
    //         }
    //     }
    // }
    // function removeAuthorityAsUser(address _bank) public hasAuthority(msg.sender) {
    //     removeAuthority(msg.sender, _bank);
    // }

    // function removeAuthorityAsBank(address _user) public hasAuthority(_user)  {
    //     removeAuthority(_user, msg.sender);
    // }

    function acceptUser(address _user)
        public
        accessToUser(_user)
        isUser(_user)
    {
        require(
            !userToInfo[_user].bankAccepted[msg.sender],
            "user is already accepted"
        );
        userToInfo[_user].banks.push(msg.sender);
        userToInfo[_user].bankAccepted[msg.sender] = true;
        emit BankAcceptRequest(_user, msg.sender);
    }

    function seeMyInfo()
        public
        view
        isUser(msg.sender)
        returns (
            bool,
            string memory,
            string memory,
            string memory,
            string memory,
            address[] memory
        )
    {
        UserInfo memory userInfo = userToInfo[msg.sender];
        return (
            userInfo.exists,
            userInfo.name,
            userInfo.drivingLicense,
            userInfo.utilityBill,
            userInfo.passport,
            userInfo.banks
        );
    }

    function seeUserInfo(address _user)
        public
        view
        accessToUser(_user)
        returns (
            bool,
            string memory,
            string memory,
            string memory,
            string memory
        )
    {
        UserInfo memory userInfo = userToInfo[_user];
        return (
            userInfo.exists,
            userInfo.name,
            userInfo.drivingLicense,
            userInfo.utilityBill,
            userInfo.passport
        );
    }
}
